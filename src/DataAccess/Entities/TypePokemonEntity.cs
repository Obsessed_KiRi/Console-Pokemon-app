﻿namespace DataAccess.Entities;
public class TypePokemonEntity
{
    public int Id { get; set; }
    public int Slot { get; set; }
    public int PokemonEntityId { get; set; }
    public PokemonEntity? Pokemon { get; set; }

    public int TypeEntityId { get; set; }
    public TypeEntity? Type { get; set; }
}
