﻿namespace DataAccess.Entities;

public class PokemonEntity
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public float Height { get; set; }
    public bool IsDefault { get; set; }
    public int Order { get; set; }
    public int Weight { get; set; }
    public List<TypePokemonEntity> Types { get; set; } = new();
}