﻿using Microsoft.EntityFrameworkCore;
using DataAccess.Entities;

namespace DataAccess.Database;

public class DatabaseContext : DbContext
{
    private readonly string? _connectionString;
    public DatabaseContext(string connectionString)
    {
        _connectionString = connectionString;
    }
    public DbSet<PokemonEntity> Pokemons { get; set; } = null!;
    public DbSet<TypeEntity> Types { get; set; } = null!;
    public DbSet<TypePokemonEntity> TypePokemons { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(_connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new PokemonConfig());
        modelBuilder.ApplyConfiguration(new TypePokemonConfig());
        modelBuilder.ApplyConfiguration(new TypeConfig());
    }
}