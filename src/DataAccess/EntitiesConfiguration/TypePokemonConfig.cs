﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DataAccess.Entities;

namespace DataAccess.Database;

public class TypePokemonConfig : IEntityTypeConfiguration<TypePokemonEntity>
{
    public void Configure(EntityTypeBuilder<TypePokemonEntity> builder)
    {
        builder
           .ToTable("TypePokemons");
        builder.HasKey(tp => tp.Id);

        builder.HasOne(tp => tp.Type)
               .WithMany()
               .HasForeignKey(tp => tp.TypeEntityId);
    }
}
