﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Database;

public class PokemonConfig : IEntityTypeConfiguration<PokemonEntity>
{
    public void Configure(EntityTypeBuilder<PokemonEntity> builder)
    {
        builder
           .ToTable("Pokemon");
        builder.HasKey(pe => pe.Id);

        builder.HasMany(pe => pe.Types)
               .WithOne(tp => tp.Pokemon)
               .HasForeignKey(tp => tp.PokemonEntityId);
    }
}
