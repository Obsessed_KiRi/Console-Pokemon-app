﻿using DataAccess.Database;
using DataAccess.Entities;

namespace DataAccess;

public class PokemonRepo
{
    public PokemonEntity GetPokemonInfoFromDb(string connectionString, string name)
    {
        using (var context = new DatabaseContext(connectionString))
        {
            var pokemonInfo = context.Pokemons
                .Where(p => p.Name == name)
                .Select(p => new PokemonEntity
                {
                    Id = p.Id,
                    Name = p.Name,
                    Height = p.Height,
                    IsDefault = p.IsDefault,
                    Order = p.Order,
                    Weight = p.Weight,
                    Types = p.Types.Select(type => new TypePokemonEntity
                    {
                        Slot = type.Slot,
                        Type = new TypeEntity
                        {
                            Name = type.Type.Name,
                            Url = type.Type.Url
                        }
                    }).ToList()
                }).FirstOrDefault();

            return pokemonInfo;
        }
    }
}
