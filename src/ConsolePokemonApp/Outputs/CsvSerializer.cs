﻿using System.Collections.Generic;
using CsvHelper;
using System.Globalization;
using System.IO;

namespace PokeApp;

public class CsvSerializer : IDataSerializer
{
    public void DataSerialize<T>(T pokemon, string path)
    {
        using var sw = new StreamWriter(path);
        using var csv = new CsvWriter(sw, CultureInfo.InvariantCulture);
        csv.WriteRecords(new List<T> {pokemon});
    }
}
