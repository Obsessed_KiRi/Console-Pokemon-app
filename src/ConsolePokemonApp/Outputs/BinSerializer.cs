﻿using System.Runtime.Serialization.Formatters.Binary;

namespace PokeApp;

public class BinSerializer : IDataSerializer 
{ 
    public void DataSerialize<T>(T pokemon, string path)
    {
#pragma warning disable SYSLIB0011
        using var fs = new FileStream(path, FileMode.Create);
        var formatter = new BinaryFormatter();
        formatter.Serialize(fs, pokemon);
    } 
}