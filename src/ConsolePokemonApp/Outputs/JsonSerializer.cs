﻿using Newtonsoft.Json;

namespace PokeApp;

public class JsonSerializer : IDataSerializer
{
    public void DataSerialize<T>(T content, string path)
    {
        var jsonFormat = JsonConvert.SerializeObject(content, Formatting.Indented);
        File.WriteAllText(path, jsonFormat);
    }
}