﻿using System.IO;
using System.Xml.Serialization;

namespace PokeApp;

public class XmlFormatSerializer : IDataSerializer
{
    public void DataSerialize<T>(T data, string path)
    {
        var serializer = new XmlSerializer(typeof(T));
        using var sw = new StreamWriter($"{path}.xml");
        serializer.Serialize(sw, data);
    }
}