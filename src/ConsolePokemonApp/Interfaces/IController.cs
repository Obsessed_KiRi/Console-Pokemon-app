﻿namespace PokeApp;
public interface IController
{
   Task PullData(string name, string path);
}