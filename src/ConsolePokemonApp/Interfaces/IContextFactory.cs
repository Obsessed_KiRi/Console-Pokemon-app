﻿using PokemonAPI;

namespace PokeApp;

public interface IContextFactory
{
    IController CreateController(IPokemonDataClient service);
}