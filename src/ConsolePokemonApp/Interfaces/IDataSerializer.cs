﻿namespace PokeApp;
public interface IDataSerializer
{
    void DataSerialize<T>(T data, string fileName);
}