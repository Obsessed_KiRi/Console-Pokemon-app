﻿using McMaster.Extensions.CommandLineUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PokeApp;

[Command(Description = "App for getting information about Pokemon")]
[Subcommand(typeof(PokeCommand))]
[Subcommand(typeof(SaveDataToDbCommand))]
[Subcommand(typeof(SearchCommand))]
[HelpOption]
class Program
{
    static void Main(string[] args)
    {
        try
        {
            var services = new ServiceCollection();
            var serviceConfig = new ServiceConfig();
            serviceConfig.Configure(services);
            var serviceProvider = services.BuildServiceProvider();

            var app = new CommandLineApplication<Program>();
            app.Conventions
                .UseDefaultConventions()
                .UseConstructorInjection(serviceProvider);
         
            app.Execute(args);
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Error no access to file. {ex.Message}");
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine($"Error file not found. {ex.Message}");
        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine($"Error http request failed. {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error occurred: {ex.Message}");
        }
    }
}
