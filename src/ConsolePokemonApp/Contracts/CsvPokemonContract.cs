﻿namespace PokeApp;

public class CsvPokemonContract
{
    public string Name { get; set; }
    public int Id { get; set; }
    public float Height { get; set; }
    public bool IsDefault { get; set; }
    public string Description { get; set; }
}
