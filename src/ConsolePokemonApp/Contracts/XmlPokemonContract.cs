﻿namespace PokeApp;

public class XmlPokemonContract
{
    public string Name { get; set; }
    public int Id { get; set; }
}