﻿using PokemonAPI;

namespace PokeApp;
public class CsvContextFactory : IContextFactory
{
    public IController CreateController(IPokemonDataClient service)
    {
        return new CsvController(service, new CsvSerializer());
    }
}