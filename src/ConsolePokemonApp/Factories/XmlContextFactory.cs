﻿using PokemonAPI;

namespace PokeApp;
public class XmlContextFactory : IContextFactory
{
    public IController CreateController(IPokemonDataClient service)
    {
        return new XmlController(service, new XmlFormatSerializer());
    }
}