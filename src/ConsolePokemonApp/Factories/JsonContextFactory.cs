﻿using PokemonAPI;

namespace PokeApp;

public class JsonContextFactory : IContextFactory
{
    public IController CreateController(IPokemonDataClient service)
    {
        return new JsonController(service, new JsonSerializer());
    }
}