﻿using PokemonAPI;

namespace PokeApp;

public class BinContextFactory : IContextFactory
{
    public IController CreateController(IPokemonDataClient service)
    {
        return new BinController(service, new JsonSerializer());
    }
}