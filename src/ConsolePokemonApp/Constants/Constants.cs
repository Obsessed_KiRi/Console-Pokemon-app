﻿namespace PokeApp;

public static class Constants
{
    public const string CsvFileType = "csv";
    public const string XmlFileType = "xml";
    public const string JsonFileType = "json";
    public const string BinFileType = "bin";
}