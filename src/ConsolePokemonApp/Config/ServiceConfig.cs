﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PokemonAPI;
using BusinessLogic;
using DataAccess;

namespace PokeApp;

public class ServiceConfig
{
    public void Configure(IServiceCollection services)
    {
        services.AddSingleton<ConsoleOutputter>();
        services.AddSingleton<PokemonRepo>();
        services.AddTransient<PokemonFacade>();
        services.AddTransient<IPokemonDataClient, PokemonDataClient>();
        services.AddSingleton<XmlContextFactory>();
        services.AddSingleton<JsonContextFactory>();
        services.AddSingleton<CsvContextFactory>();
        services.AddSingleton<BinContextFactory>();
    }
}