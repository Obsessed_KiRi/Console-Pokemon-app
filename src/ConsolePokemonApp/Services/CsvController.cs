﻿using PokemonAPI;

namespace PokeApp;

public class CsvController : IController
{
    private readonly IPokemonDataClient _getPokemonData;
    private readonly IDataSerializer _dataSerializer;

    public CsvController(IPokemonDataClient getPokemonData, IDataSerializer dataSerializer)
    {
        _getPokemonData = getPokemonData;
        _dataSerializer = dataSerializer;
    }

    public async Task PullData(string name, string path)
    {
        var pokemonInfo = _getPokemonData.GetPokemonInfoAsync(name).Result;
        var description = $"{pokemonInfo.Name} ({pokemonInfo.Id}): {pokemonInfo.Height}";

        var data = new CsvPokemonContract()
        {
            Name = pokemonInfo.Name,
            Id = pokemonInfo.Id,
            Height = pokemonInfo.Height,
            IsDefault = pokemonInfo.IsDefault,
            Description = description,
        };
        _dataSerializer.DataSerialize(data, path);
    }
}