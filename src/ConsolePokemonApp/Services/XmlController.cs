﻿using PokemonAPI;

namespace PokeApp;

public class XmlController : IController
{
    private readonly IPokemonDataClient _getPokemonData;
    private readonly IDataSerializer _dataSerializer;

    public XmlController(IPokemonDataClient getPokemonData, IDataSerializer dataSerializer)
    {
        _getPokemonData = getPokemonData;
        _dataSerializer = dataSerializer;
    }

    public async Task PullData(string name, string path)
    {
        var pokemonInfo = await _getPokemonData.GetPokemonInfoAsync(name);
        var data = new XmlPokemonContract
        {
            Name = pokemonInfo.Name,
            Id = pokemonInfo.Id
        };

        _dataSerializer.DataSerialize(data, path);
    }
}