﻿using Spectre.Console;
using DataAccess.Entities;

namespace PokeApp;

public class ConsoleOutputter
{
    public void DisplayPokemonInfo(PokemonEntity pokemon)
    {
        try
        {
            var table = new Table();
            table.AddColumn(new TableColumn("Pokemon property").Header("[magenta1]Property[/]").Centered());
            table.AddColumn(new TableColumn("Property value").Header("[cyan1]Value[/]").Centered());
            table.AddRow("ID", pokemon.Id.ToString());
            table.AddRow("Name", pokemon.Name);
            table.AddRow("Height", pokemon.Height.ToString());
            table.AddRow("Weight", pokemon.Weight.ToString());
            table.AddRow("Default", pokemon.IsDefault ? "Yes" : "No");
            table.AddRow("Order", pokemon.Order.ToString());

            var typesTable = new Table();
            typesTable.AddColumns("Slot", "Type", "URL");
            foreach (var type in pokemon.Types)
            {
                typesTable.AddRow(type.Slot.ToString(), type.Type.Name, type.Type.Url);
            }
            AnsiConsole.Write(table);
            AnsiConsole.Write(typesTable);
        }
        catch(Exception ex)
        {
            AnsiConsole.MarkupLine($"[red]Data for this Pokemon was not found, try entering the name of another Pokemon[/] \nMore information about error: {ex.Message}");
            throw;
        }
    }
}
