﻿using PokemonAPI.Old.Contracts;
using PokemonAPI;

namespace PokeApp;
public class JsonController : IController
{
    private readonly IPokemonDataClient _getPokemonData;
    private readonly IDataSerializer _dataSerializer;

    public JsonController(IPokemonDataClient getPokemonData, IDataSerializer dataSerializer)
    {
        _getPokemonData = getPokemonData;
        _dataSerializer = dataSerializer;
    }

    public async Task PullData(string name, string path)
    {
        var pokemonInfo = await _getPokemonData.GetPokemonInfoAsync(name);
        var data = new Pokemon
        {
            Name = pokemonInfo.Name,
            Id = pokemonInfo.Id,
            Height = pokemonInfo.Height,
            IsDefault = pokemonInfo.IsDefault,
            Types = pokemonInfo.Types,
        };

        _dataSerializer.DataSerialize(data, path);
    }
}
