﻿using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using BusinessLogic;
using Spectre.Console;

namespace PokeApp;

[Command(Name = "sync", Description = "Save pokemon name to database")]
public class SaveDataToDbCommand
{
    private readonly PokemonFacade _pokemonFacade;
    public SaveDataToDbCommand(PokemonFacade pokemonFacade)
    {
        _pokemonFacade = pokemonFacade;
    }

    [Required, Option("-c|--connection <connection_string>", "Connection string for database", CommandOptionType.SingleValue)]
    public string ConnectionString { get; set; }

    protected async Task OnExecuteAsync()
    {
        try
        {
            await _pokemonFacade.SyncPokemonDataToDatabaseAsync(ConnectionString);
            AnsiConsole.MarkupLine("[lime]Data saved to db successfully![/]");
        }
        catch (Exception ex)
        {
            AnsiConsole.MarkupLine($"[red]Error occurred: {ex.Message}[/]");
            if (ex.InnerException != null)
            {
                AnsiConsole.MarkupLine($"[red]Inner exception: {ex.InnerException.Message}[/]");
            }
        }
    }
}