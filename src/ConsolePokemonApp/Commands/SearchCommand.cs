﻿using DataAccess;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;

namespace PokeApp;

[Command(Name = "search", Description = "Search pokemon by the name")]
public class SearchCommand
{
    private readonly PokemonRepo _pokemonRepo;
    private readonly ConsoleOutputter _consoleOutputter;
    public SearchCommand(PokemonRepo pokemonRepo, ConsoleOutputter consoleOutputter)
    {
        _pokemonRepo = pokemonRepo;
        _consoleOutputter = consoleOutputter;
    }
    [Required, Option("-n <name>", "Search Pokémon by the name", CommandOptionType.SingleValue)]
    private string PokemonName { get; set; }

    [Required, Option("-cs <connection_string>", "Enter your connection string for database", CommandOptionType.SingleValue)]
    private string ConnectionString { get; set; }
    protected void OnExecute()
    {
        var pokemonRepo = _pokemonRepo.GetPokemonInfoFromDb(ConnectionString, PokemonName);
        _consoleOutputter.DisplayPokemonInfo(pokemonRepo);
    }
}