﻿using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;
using PokemonAPI;

namespace PokeApp;

[Command(Name = "info", Description = "Showing information about Pokemon")]
public class PokeCommand
{
    private readonly IPokemonDataClient _pokemonService;
    private readonly IServiceProvider _serviceProvider;

    public PokeCommand(IPokemonDataClient pokemonService, IServiceProvider serviceProvider)
    {
        _pokemonService = pokemonService;
        _serviceProvider = serviceProvider;
    }

    [Argument(0, Description = "Name of the Pokemon")]
    private string PokemonName { get; set; }

    [Option("-t|--type <type>", "Type of file output: json, xml, csv, bin. By default is json.", CommandOptionType.SingleValue)]
    private string FileType { get; set; } = Constants.JsonFileType;

    [Required, Option("-n|--name <name>", "Name of the file for data output.", CommandOptionType.SingleValue)]
    private string FileName { get; set; }

    protected virtual async Task OnExecute()
    {
        IContextFactory cf = CreateContextFactory(FileType);

        IController controller = cf.CreateController(_pokemonService);

        await controller.PullData(PokemonName, FileName);

        AnsiConsole.MarkupLine($"[lime]Data saved to {FileName} successfully![/]");
    }

    private IContextFactory CreateContextFactory(string fileType)
    {
        IContextFactory cf = fileType.ToLower() switch
        {
            Constants.JsonFileType => _serviceProvider.GetRequiredService<JsonContextFactory>(),
            Constants.XmlFileType => _serviceProvider.GetRequiredService<XmlContextFactory>(),
            Constants.CsvFileType => _serviceProvider.GetRequiredService<CsvContextFactory>(),
            Constants.BinFileType => _serviceProvider.GetRequiredService<BinContextFactory>(),
            _ => throw new ArgumentException($"Unknown file type: {fileType}")
        };
        return cf;
    }
}