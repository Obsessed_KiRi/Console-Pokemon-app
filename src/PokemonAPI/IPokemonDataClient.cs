﻿using PokemonAPI.Contracts;
using PokemonAPI.Old.Contracts;

namespace PokemonAPI;

public interface IPokemonDataClient
{
    Task<Pokemon> GetPokemonInfoAsync(string name);
    Task<List<PokemonEndpoint>> GetPokemonListAsync();
    Task<PokemonDetails> GetDetailedPokemonDataAsync(PokemonEndpoint pokemon);
}
