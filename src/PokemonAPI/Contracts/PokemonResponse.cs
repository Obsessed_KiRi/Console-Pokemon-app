﻿using Newtonsoft.Json;

namespace PokemonAPI.Contracts;

public class PokemonResponse
{
    [JsonProperty("results")]
    public List<PokemonEndpoint> Results { get; set; } = new();
}
