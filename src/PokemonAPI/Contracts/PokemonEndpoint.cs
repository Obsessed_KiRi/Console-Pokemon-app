﻿namespace PokemonAPI.Contracts;

public class PokemonEndpoint
{
    public string? Name { get; set; }
    public string? Url { get; set; }
    public PokemonDetails Details { get; set; } = new();
}

