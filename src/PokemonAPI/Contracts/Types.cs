﻿namespace PokemonAPI.Contracts;

public class Types
{
    public int Slot { get; set; }
    public TypesDetails Type { get; set; } = new();
}