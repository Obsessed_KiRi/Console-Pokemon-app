﻿using Newtonsoft.Json;

namespace PokemonAPI.Contracts;

public class PokemonDetails
{
    public int Id { get; set; }
    public string Name { get; set; }
    public float Height { get; set; }
    [JsonProperty("is_default")]
    public bool IsDefault { get; set; }
    public int Order { get; set; }
    public int Weight { get; set; }
    public List<Types> Types { get; set; } = new();
}