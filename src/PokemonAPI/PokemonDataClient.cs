﻿using Newtonsoft.Json;
using PokemonAPI.Contracts;
using PokemonAPI.Old.Contracts;

namespace PokemonAPI;

public class PokemonDataClient : IPokemonDataClient
{
    private readonly HttpClient _httpClient = new();
    private static readonly string ApiUrl = "https://pokeapi.co/api/v2/";

    public async Task<Pokemon> GetPokemonInfoAsync(string name)
    {
        HttpResponseMessage response = await _httpClient.GetAsync($"{ApiUrl}pokemon?limit=20");
        response.EnsureSuccessStatusCode();

        var pokemon = await response.Content.ReadAsAsync<Pokemon>();
        return pokemon;
    }

    public async Task<List<PokemonEndpoint>> GetPokemonListAsync()
    {
        HttpResponseMessage response = await _httpClient.GetAsync($"{ApiUrl}pokemon?limit=20");
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        var pokemonResponse = JsonConvert.DeserializeObject<PokemonResponse>(responseString);

        return pokemonResponse.Results;
    }

    public async Task<PokemonDetails> GetDetailedPokemonDataAsync(PokemonEndpoint pokemon)
    {
        HttpResponseMessage response = await _httpClient.GetAsync(pokemon.Url);
        response.EnsureSuccessStatusCode();

        var contentResponse = await response.Content.ReadAsStringAsync();
        var pokemonDetails = JsonConvert.DeserializeObject<PokemonDetails>(contentResponse);

        return pokemonDetails;
    }
}