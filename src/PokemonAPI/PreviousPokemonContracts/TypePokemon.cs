﻿namespace PokemonAPI.Old.Contracts;

[Serializable]
public class TypePokemon
{
    public int Slot { get; set; }
    public TypeDetail Type { get; set; }
}