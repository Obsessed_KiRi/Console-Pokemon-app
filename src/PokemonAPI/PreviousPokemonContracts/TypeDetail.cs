﻿namespace PokemonAPI.Old.Contracts;

[Serializable]
public class TypeDetail
{
    public string Name { get; set; }
    public string Url { get; set; }
}