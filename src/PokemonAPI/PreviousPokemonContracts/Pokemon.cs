﻿namespace PokemonAPI.Old.Contracts;

[Serializable]
public class Pokemon
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public float Height { get; set; }
    public bool IsDefault { get; set; }
    public int Order { get; set; }
    public int Weight { get; set; }
    public List<TypePokemon> Types { get; set; } = new();
}