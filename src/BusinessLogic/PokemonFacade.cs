﻿using PokemonAPI;
using DataAccess.Database;
using Spectre.Console;
using DataAccess.Entities;

namespace BusinessLogic;

public class PokemonFacade
{
    private IPokemonDataClient _pokemonDataClient;
    public PokemonFacade(IPokemonDataClient pokemonDataClient)
    {
        _pokemonDataClient = pokemonDataClient;
    }
    public async Task SyncPokemonDataToDatabaseAsync(string connectionString)
    {
        using var db = new DatabaseContext(connectionString);

        db.Database.EnsureDeleted();
        db.Database.EnsureCreated();

        var pokemonEndpoints = await _pokemonDataClient.GetPokemonListAsync();
        var pokemonEntities = new List<PokemonEntity>();

        foreach (var endpoint in pokemonEndpoints)
        {
            var details = await _pokemonDataClient.GetDetailedPokemonDataAsync(endpoint);
            pokemonEntities.Add(new PokemonEntity
            {
                Name = endpoint.Name,
                Height = details.Height,
                IsDefault = details.IsDefault,
                Order = details.Order,
                Weight = details.Weight,
                Types = details.Types.Select(type => new TypePokemonEntity
                {
                    Slot = type.Slot,
                    Type = new TypeEntity
                    {
                        Name = type.Type.Name,
                        Url = type.Type.Url
                    }
                }).ToList()
            });
        }
        await db.Pokemons.AddRangeAsync(pokemonEntities);
        await db.SaveChangesAsync();
    }
}
